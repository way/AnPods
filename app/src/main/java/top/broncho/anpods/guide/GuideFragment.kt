package top.broncho.anpods.guide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_guide_normal.*
import top.broncho.anpods.R


class GuideFragment : Fragment() {
    private val position by lazy { arguments?.getInt(KEY_POSITION, 0) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutId =
            if (position == NUM_PAGES - 1) R.layout.fragment_guide_end else R.layout.fragment_guide_normal
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        guideIV?.apply {
            if (position == 0) {
                setAnimation(R.raw.data4)
                playAnimation()
                return
            }
            val imageId = when (position) {
                0 -> R.drawable.ic_case
                1 -> R.drawable.ic_battery
                //2 -> R.drawable.ic_location
                2 -> R.drawable.ic_floating
                else -> R.drawable.ic_bluetooth
            }
            setImageResource(imageId)
        }
        guideTitleTV?.apply {
            val stringId = when (position) {
                0 -> R.string.guide_title_one
                1 -> R.string.guide_title_battery
                //2 -> R.string.guide_title_location
                2 -> R.string.guide_title_float
                else -> R.string.guide_title_bluetooth
            }
            setText(stringId)
        }
        guideContentTV?.apply {
            val stringId = when (position) {
                0 -> R.string.guide_content_one
                1 -> R.string.guide_content_battery
                //2 -> R.string.guide_content_location
                2 -> R.string.guide_content_float
                else -> R.string.guide_content_bluetooth
            }
            setText(stringId)
        }
    }

    companion object {
        private const val KEY_POSITION = "position"
        fun newInstance(position: Int): GuideFragment {
            val bundle = Bundle().apply { putInt(KEY_POSITION, position) }
            return GuideFragment().apply {
                if (arguments != null) arguments?.putAll(bundle) else arguments = bundle
            }
        }
    }
}

package top.broncho.anpods.ui.widget

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.layout_permission_dialog.*
import org.jetbrains.anko.AnkoLogger
import top.broncho.anpods.BuildConfig
import top.broncho.anpods.R


class PermissionDialog @JvmOverloads constructor(
    context: Context
) : BottomSheetDialog(context), AnkoLogger {

    init {
        setContentView(R.layout.layout_permission_dialog)
        btn_close.setOnClickListener {
            onBackPressed()
        }
        btn_settings.setOnClickListener {
            context.startActivity(
                Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:${BuildConfig.APPLICATION_ID}")
                )
            )
        }
    }

}
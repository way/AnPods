package top.broncho.anpods

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startService

class AutoStarter : BroadcastReceiver(), AnkoLogger {
    override fun onReceive(context: Context?, intent: Intent?) {
        info { "onReceive: ${intent?.action}" }
        context?.startService<AnPodsService>()
    }
}
package top.broncho.anpods

import android.app.Application
import android.content.Context
import com.tencent.bugly.Bugly
import com.tencent.bugly.beta.Beta
import org.jetbrains.anko.AnkoLogger
import kotlin.properties.Delegates


class App : Application(), AnkoLogger {

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        /**
         * true表示初始化时自动检查升级; false表示不会自动检查升级,需要手动调用Beta.checkUpgrade()方法;
         */
        Beta.autoCheckUpgrade = true
        Beta.upgradeDialogLayoutId = R.layout.layout_upgrade_dialog
        Bugly.init(applicationContext, BuildConfig.BUGLY_APPID, BuildConfig.DEBUG)
    }

    companion object {
        var context: Context by Delegates.notNull()
    }

}